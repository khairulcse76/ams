<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

class Comment extends Model
{
 use SoftDeletes;

 protected $fillable= ['details',];

 protected $dates=['deleted_at'];

    public static function boot()
    {
        parent::boot();

        static::creating(function($tag)
        {
            if (Auth::check()) {
                $tag->user_id = Auth::user()->id;
            }
        });
    }

    public function user(){

        return $this->belongsTo('App\User');
    }

    public function commentable()
    {
        return $this->morphTo();
    }
}
