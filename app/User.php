<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Pondit\Authorize\Models\Authorizable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use SoftDeletes, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'profession_id', 'name', 'email', 'password','remember_token',

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    /**
     * Get the profile record associated with the user.
     */
    public function profile()
    {
        return $this->hasOne('App\Profile');
    }

    /**
     * Get the type of the user.
     */
    public function profession()
    {
        return $this->belongsTo('App\Profession');
    }

    /**
     * Get the type of the user.
     */
    public function articles()
    {
        return $this->hasMany('App\Article');
    }

    /**
     * Get the type of the user.
     */
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
}
