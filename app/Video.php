<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Video extends Model
{
    use SoftDeletes;

    protected $fillable=[ 'provider', 'title', 'summary', 'source', 'display'];

    protected $guarded=['id', 'created_at', 'updated_at', 'deleted_at',];

    protected $dates = ['deleted_at'];



    public function galleries()
    {
        return $this->belongsToMany('App\Gallery');
    }


    /**
     * Get all of the tags for the video.
     */
    public function tags()
    {
        return $this->morphToMany('App\Tag', 'taggable');
    }

    /**
     * Get all of the video's comments.
     */
    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }
}
