<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{
    use SoftDeletes;

    protected $fillable=['name', 'slug',];

    protected $dates=['deleted_at'];

    public static function boot()
    {
        parent::boot();

        static::creating(function($tag)
        {
            $tag->slug = utf8_slug($tag->name);
            if (Auth::check()) {
                $tag->created_by = Auth::user()->id;
            }
        });

        static::updating(function ($tag)
        {
            $tag->slug = utf8_slug($tag->name);
        });

        static::deleting(function($tag)
        {
            if (Auth::check()) {
                $tag->deleted_by = Auth::user()->id;
                $tag->save();
            }
        });
    }



    public function creator()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    /**
     * Get the user who delete the tag.
     */
    public function remover()
    {
        return $this->belongsTo('App\User', 'deleted_by');
    }

    /**
     * Get all of the articles that are assigned this tag.
     */
    public function articles()
    {
        return $this->morphedByMany('App\Article', 'taggable');
    }

    /**
     * Get all of the videos that are assigned this tag.
     */
    public function videos()
    {
        return $this->morphedByMany('App\Video', 'taggable');
    }
}
