<?php

namespace App;
use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id', 'subject_id', 'title', 'slug', 'sub_title', 'summary', 'details', 'display', 'image'];

    protected $guarded=[ 'id', 'created_at', 'updated_at', 'deleted_at'];


    public static function boot()
    {
        parent::boot();

        static::creating(function($article)
        {
            if (Auth::check()) {
                $article->user_id = Auth::user()->id;
            }
            $article->slug = utf8_slug($article->title);
        });

        static::updating(function ($article)
        {
            $article->slug = utf8_slug($article->title);
        });

    }
    //relationship model relationship
    //relationship model relationship
    //relationship model relationship
    //relationship model relationship

    public function user(){
            return $this->belongsTo('App\User');
    }
    public function subject()
    {
        return $this->belongsTo('App\Subject');
    }

    public function tags()
    {
        return $this->morphToMany('App\Tag', 'taggable');
    }

    public function comments()
    {
        return $this->morphMany('App\Models\Comment', 'commentable');
    }


}
