<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profession extends Model
{
    use SoftDeletes;

    protected $fillable=['name'];

    protected $dates=['deleted_at'];

    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function articles()
    {
        return $this->hasManyThrough('App\Article', 'App\User');
    }
}
