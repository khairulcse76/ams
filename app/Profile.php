<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model
{
    use SoftDeletes;

    protected $fillable=['user_id', 'picture', 'bio', 'web', 'facebook', 'twitter', 'github'];

    protected $dates=['deleted_at'];

    protected $guarded=['id', 'created_at', 'updated_at'];

    public function user(){

        return $this->belongsTo('App\User');
    }
}
