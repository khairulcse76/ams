<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subject extends Model
{
    use SoftDeletes;

    protected $fillable=['name', 'description',];

    protected $dates=['deleted_at'];

    public static function boot()
    {
        parent::boot();

        static::creating(function($subject)
        {
            $subject->slug = utf8_slug($subject->name);
            if (Auth::check()) {
                $subject->created_by = Auth::user()->id;
            }
        });

        static::updating(function ($subject)
        {
            $subject->slug = utf8_slug($subject->name);
        });

        static::deleting(function($subject)
        {
            if (Auth::check()) {
                $subject->deleted_by = Auth::user()->id;
                $subject->save();
            }
        });
    }

    public function creator()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    /**
     * Get the user who delete the subject.
     */
    public function remover()
    {
        return $this->belongsTo('App\User', 'deleted_by');
    }

    /**
     * Get the articles for the subject.
     */
    public function articles()
    {
        return $this->hasMany('App\Article');
    }
}
