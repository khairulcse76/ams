<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Video;

class Gallery extends Model
{
    use SoftDeletes;

    protected $fillable=['name','description','display'];

    protected $dates=['deleted_at'];
public function videos(){

    return $this->belongsToMany('App\Video');
}
}
