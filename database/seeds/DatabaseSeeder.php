<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{

    public function run()
    {
        $this->call(ProfessionsTableSeeder::class);
        //Seeding using Model Factory with relation
        $this->call(UsersTableSeeder::class);
        //Seeding using faker in Seeder
        $this->call(SubjectsTableSeeder::class);

        $this->call(TagsTableSeeder::class);

        $this->call(ArticlesTableSeeder::class);

        $this->call(GalleriesAndVideosTableSeeder::class);

        $this->call(CommentsTableSeeder::class);

        $this->call(TaggablesTableSeeder::class);

    }
}