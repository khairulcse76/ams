<?php

$factory->define(App\Profile::class, function (Faker\Generator $faker) {
    return [
        'picture' => $faker->image(public_path('uploads/profile-picture'), 200, 300, 'people', false),
        'bio' => $faker->paragraph(5),
        'web' => $faker->url,
        'facebook' => 'https://www.facebook.com/' . $faker->unique()->firstName,
        'twitter' => 'https://www.twitter.com/' . $faker->unique()->firstName,
        'github' => 'https://www.github.com/' . $faker->unique()->lastName,
    ];
});
