<?php

$factory->define(App\Comment::class, function (Faker\Generator $faker) {
    return [
        'user_id' => $faker->randomElement(App\User::pluck('id')->toArray()),
        'details' => $faker->text(220),
        'created_at' => $faker->dateTimeThisYear()
    ];
});
