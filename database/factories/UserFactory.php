<?php

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'profession_id' => $faker->numberBetween(1, 5),
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = 'secret',
        'remember_token' => str_random(10),
    ];
});